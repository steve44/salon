create database salon;
use salon;

create table coupe
(
	Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Lieu varchar(32),
	Disponibilite varchar(32),
	Heure varchar(32)
);


create table esthetique
(
	Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Lieu varchar(32),
	Disponibilite varchar(32),
	Heure varchar(32)
);


create table administrateur
(
	Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Nom varchar(32),
	Pass varchar(32)
);

insert into administrateur (Nom,Pass) values ('root','root');
insert into administrateur (Nom,Pass) values ('steve','steve44');