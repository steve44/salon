-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 06 avr. 2018 à 14:26
-- Version du serveur :  10.1.31-MariaDB
-- Version de PHP :  7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `id5306012_beaute`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE `administrateur` (
  `Id` int(11) NOT NULL,
  `Nom` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Pass` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`Id`, `Nom`, `Pass`) VALUES
(1, 'root', 'root'),
(2, 'steve', 'steve44');

-- --------------------------------------------------------

--
-- Structure de la table `coupe`
--

CREATE TABLE `coupe` (
  `Id` int(11) NOT NULL,
  `Lieu` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Disponibilite` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Heure` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `coupe`
--

INSERT INTO `coupe` (`Id`, `Lieu`, `Disponibilite`, `Heure`) VALUES
(1, 'Yannick beaute,Ampaspito', 'lundi - samedi', '9h - 18h'),
(2, 'tsiky Beaute,analakely', 'mardi - samedi', '9h - 18h');

-- --------------------------------------------------------

--
-- Structure de la table `esthetique`
--

CREATE TABLE `esthetique` (
  `Id` int(11) NOT NULL,
  `Lieu` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Disponibilite` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Heure` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `esthetique`
--

INSERT INTO `esthetique` (`Id`, `Lieu`, `Disponibilite`, `Heure`) VALUES
(1, 'Charmelle Beaute,Behoririka', 'lundi - samedi', '9h - 18h');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `coupe`
--
ALTER TABLE `coupe`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `esthetique`
--
ALTER TABLE `esthetique`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `administrateur`
--
ALTER TABLE `administrateur`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `coupe`
--
ALTER TABLE `coupe`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `esthetique`
--
ALTER TABLE `esthetique`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
